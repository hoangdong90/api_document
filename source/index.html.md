---
title: API Reference

language_tabs:
  - shell
  - ruby
  - python
  - javascript

toc_footers:
  - <a href='https://www.boxme.vn/'>Edit by Boxme/Shipchung</a>

includes:
  - location
  - product
  - shipment
  - orders
  - manifest
  - errors

search: true
---

# Introduction

## About us

Boxme Global is one-stop solution for Cross-Border E-Commerce seller, who want to become millionaire.

Shipchung is a member of Boxme Global provide shipping gateway for ecommerce.

## API Key

How to get your API Key?

Before you started with using our REST API services, you will need to obtain your API key under your own user account.

Login to your Shipchung.vn account (Sign up free) Visit Seller center > Setting > API Key or click this link
-  Shipchung Vietnam: https://seller.shipchung.vn/#/app/config/key
-  Boxme Vietnam: https://seller.boxme.vn/#/app/config/key
-  Boxme Global: https://seller.boxme.asia/#/app/config/key
-  Click Generate API Key

## API end point

Once you've registered your API it's easy to start requesting data from Shipchung.

All endpoints are only accessible via https

-  Domain sandbox: http://prod.boxme.vn/

-  User: sandbox@shipchung.vn Pass: 123456

-  Production API:

-  Boxme Seller Client: http://services.boxme.vn

-  Boxme Warehouse Client: http://wms.boxme.vn

-  Shipchung client: https://services.shipchung.vn

## Header

All API call must include this header in order to auth the usage of Shipchung API.

Replace your API_API_KEY with your own key. Shipchung-api-key: YOUR_API_KEY Content-Type: application/json Body

All the request and response are in JSON string.
Basic Integration
Using the Shipchung API is easy! Follow this step-by-step guide to get shipping rates and create your first shipping labels in minutes.

## API Supported

### Products
- Create your product SKU to Boxme
- Edit your product SKU on Boxme
- Get your product SKU list
- Get your product inventory
- Get fuflillable product(s) list to create order

### Shipment
- Create your inbound shipment to Boxme fulfillment center.

### Location
- Get province/city list
- Get district by province/city ID
- Get wards by district ID

### Order
- Calculate/estimate shipping fee
- Create order fulfill by Boxme
- Create order fulfill by you
- Order issues (resolution):
- Confirm redelivery order
- Accept overweight order
- Confirm return or cancel order



