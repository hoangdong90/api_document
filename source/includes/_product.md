# Product
## Get Lists Inventory
> To authorize, use this code:

```ruby
require 'uri'
require 'net/http'
url = URI("http://prod.boxme.vn/api/public/bxapi/list_inventory-sdk")

http = Net::HTTP.new(url.host, url.port)

request = Net::HTTP::Get.new(url)
request["content-type"] = 'application/json'
request.body = "{}"

response = http.request(request)
puts response.read_body
```

```python
import http.client

conn = http.client.HTTPConnection("prod.boxme.vn")

payload = "{}"

headers = { 'content-type': "application/json" }

conn.request("GET", "/api/public/bxapi/list_inventory-sdk", payload, headers)

res = conn.getresponse()
data = res.read()

print(data.decode("utf-8"))
```

```shell
# With shell, you can just pass the correct header with each request
curl --request GET \
  --url http://prod.boxme.vn/api/public/bxapi/list_inventory-sdk \
  --header 'content-type: application/json' \
  --data '{}'
```

```javascript
var settings = {
  "async": true,
  "crossDomain": true,
  "url": "http://prod.boxme.vn/api/public/bxapi/list_inventory-sdk",
  "method": "GET",
  "headers": {
    "content-type": "application/json"
  },
  "processData": false,
  "data": "{}"
}

$.ajax(settings).done(function (response) {
  console.log(response);
});
```
> The above command returns JSON structured like this:

```json
{
  "inventory": [
    {
      "City": "TP. Hồ Chí Minh",
      "Code": null,
      "Name": "Kho - Huyện Nhà Bè - TP.Hồ Chí Minh",
      "District": "Huyện Nhà Bè",
      "Phone": "0938131593",
      "InventoryId": "92932",
      "Type": 0,
      "AddressLine": "aaa"
    }
  ]
}
```

### HTTP Request

`GET http://prod.boxme.vn/api/public/bxapi/list_inventory-sdk`

### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
ApiKey   | false   | Your api key of seller.

<aside class="notice">
You must with your personal API key.
</aside>

## Edit Product

> To authorize, use this code:

```ruby
require 'uri'
require 'net/http'

url = URI("http://prod.boxme.vn/api/public/bxapi/edit-product-sdk")

http = Net::HTTP.new(url.host, url.port)

request = Net::HTTP::Post.new(url)
request["content-type"] = 'application/json'
request.body = "{\"InventoryId\":\"137123\",\"ApiKey\":\"7e4e80c6bdb03c5a2d639c4828bcf156\",\"SellerSKU\":\"KA-07\",\"Name\":\"iphone 9 \",\"Description\":\"Iphone 7s black new 100%\",\"BasePrice\":10000,\"SalePrice\":20000,\"Weight\":1223,\"Volume\":\"\",\"ProductImages\":\"\"}"

response = http.request(request)
puts response.read_body
```

```python
import http.client

conn = http.client.HTTPConnection("prod.boxme.vn")

payload = "{\"InventoryId\":\"137123\",\"ApiKey\":\"7e4e80c6bdb03c5a2d639c4828bcf156\",\"SellerSKU\":\"KA-07\",\"Name\":\"iphone 9 \",\"Description\":\"Iphone 7s black new 100%\",\"BasePrice\":10000,\"SalePrice\":20000,\"Weight\":1223,\"Volume\":\"\",\"ProductImages\":\"\"}"

headers = { 'content-type': "application/json" }

conn.request("POST", "/api/public/bxapi/edit-product-sdk", payload, headers)

res = conn.getresponse()
data = res.read()

print(data.decode("utf-8"))
```

```shell
# With shell, you can just pass the correct header with each request
curl --request POST \
  --url http://prod.boxme.vn/api/public/bxapi/edit-product-sdk \
  --header 'content-type: application/json' \
  --data '{"InventoryId":"137123","ApiKey":"7e4e80c6bdb03c5a2d639c4828bcf156","SellerSKU":"KA-07","Name":"iphone 9 ","Description":"Iphone 7s black new 100%","BasePrice":10000,"SalePrice":20000,"Weight":1223,"Volume":"","ProductImages":""}'
```

```javascript
var data = JSON.stringify({
  "InventoryId": "137123",
  "ApiKey": "7e4e80c6bdb03c5a2d639c4828bcf156",
  "SellerSKU": "KA-07",
  "Name": "iphone 9 ",
  "Description": "Iphone 7s black new 100%",
  "BasePrice": 10000,
  "SalePrice": 20000,
  "Weight": 1223,
  "Volume": "",
  "ProductImages": ""
});

var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function () {
  if (this.readyState === this.DONE) {
    console.log(this.responseText);
  }
});

xhr.open("POST", "http://prod.boxme.vn/api/public/bxapi/edit-product-sdk");
xhr.setRequestHeader("content-type", "application/json");

xhr.send(data);
```
> The above command returns JSON structured like this:

```json
{
  "Name": "iphone 9 ",
  "SalePrice": 20000,
  "BasePrice": 10000,
  "SellerId": "87695",
  "Description": "Iphone 7s black new 100%",
  "QuantityUnit": null,
  "SellerSKU": "KA-07",
  "Weight": 1223,
  "Volume": null,
  "InventoryId": "137123",
  "ProductId": "88999"
}
```

### HTTP Request

`POST http://prod.boxme.vn/api/public/bxapi/edit-product-sdk`

### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
ApiKey        | false   | You Key API.
SellerSKU     | false   | Product code (sku) to edit.
Name          | false   | New product name.
InventoryId   | false   | New ID warehouse (Pickup address config).
Description   | false   | New product description .
BasePrice     | false   | New product base price  .
SalePrice     | false   | New product sale price (Price to create orders)  .
Weight        | false   | New product weight (weight to create orders)  .
Volume        | false   |  New Volume product  .

<aside class="notice">
You must with your personal API key.
</aside>

## Add Product

> To authorize, use this code:

```ruby
require 'uri'
require 'net/http'

url = URI("http://prod.boxme.vn/api/public/bxapi/product-sdk")

http = Net::HTTP.new(url.host, url.port)

request = Net::HTTP::Post.new(url)
request["content-type"] = 'application/json'
request.body = "{\"InventoryId\":\"105341\",\"ApiKey\":\"7e4e80c6bdb03c5a2d639c4828bcf156\",\"SellerSKU\":\"Test-Pro-IP7S\",\"Name\":\"IPhone 7s Black\",\"CategoryName\":\"Phone\",\"SupplierName\":\"Apple\",\"BrandName\":\"Apple\",\"Description\":\"Iphone 7s black new 100%\",\"ProductTags\":\"iphone,iphone7s,iphone7\",\"Quantity\":50,\"BasePrice\":16000000,\"SalePrice\":16500000,\"BarcodeManufacturer\":\"IP7SABC\",\"ModelName\":\"IPhone7s\",\"Weight\":200,\"Volume\":\"2x2x2\",\"ProductImages\":\"\"}"

response = http.request(request)
puts response.read_body
```

```python
import http.client

conn = http.client.HTTPConnection("prod.boxme.vn")

payload = "{\"InventoryId\":\"105341\",\"ApiKey\":\"7e4e80c6bdb03c5a2d639c4828bcf156\",\"SellerSKU\":\"Test-Pro-IP7S\",\"Name\":\"IPhone 7s Black\",\"CategoryName\":\"Phone\",\"SupplierName\":\"Apple\",\"BrandName\":\"Apple\",\"Description\":\"Iphone 7s black new 100%\",\"ProductTags\":\"iphone,iphone7s,iphone7\",\"Quantity\":50,\"BasePrice\":16000000,\"SalePrice\":16500000,\"BarcodeManufacturer\":\"IP7SABC\",\"ModelName\":\"IPhone7s\",\"Weight\":200,\"Volume\":\"2x2x2\",\"ProductImages\":\"\"}"

headers = { 'content-type': "application/json" }

conn.request("POST", "/api/public/bxapi/product-sdk", payload, headers)

res = conn.getresponse()
data = res.read()

print(data.decode("utf-8"))
```

```shell
# With shell, you can just pass the correct header with each request
curl --request POST \
  --url http://prod.boxme.vn/api/public/bxapi/product-sdk \
  --header 'content-type: application/json' \
  --data '{"InventoryId":"105341","ApiKey":"7e4e80c6bdb03c5a2d639c4828bcf156","SellerSKU":"Test-Pro-IP7S","Name":"IPhone 7s Black","CategoryName":"Phone","SupplierName":"Apple","BrandName":"Apple","Description":"Iphone 7s black new 100%","ProductTags":"iphone,iphone7s,iphone7","Quantity":50,"BasePrice":16000000,"SalePrice":16500000,"BarcodeManufacturer":"IP7SABC","ModelName":"IPhone7s","Weight":200,"Volume":"2x2x2","ProductImages":""}'
```

```javascript
var data = JSON.stringify({
  "InventoryId": "105341",
  "ApiKey": "7e4e80c6bdb03c5a2d639c4828bcf156",
  "SellerSKU": "Test-Pro-IP7S",
  "Name": "IPhone 7s Black",
  "CategoryName": "Phone",
  "SupplierName": "Apple",
  "BrandName": "Apple",
  "Description": "Iphone 7s black new 100%",
  "ProductTags": "iphone,iphone7s,iphone7",
  "Quantity": 50,
  "BasePrice": 16000000,
  "SalePrice": 16500000,
  "BarcodeManufacturer": "IP7SABC",
  "ModelName": "IPhone7s",
  "Weight": 200,
  "Volume": "2x2x2",
  "ProductImages": ""
});

var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function () {
  if (this.readyState === this.DONE) {
    console.log(this.responseText);
  }
});

xhr.open("POST", "http://prod.boxme.vn/api/public/bxapi/product-sdk");
xhr.setRequestHeader("content-type", "application/json");

xhr.send(data);
```
> The above command returns JSON structured like this:

```json
{
  "WeightUnit": 1,
  "BrandName": "brank",
  "SalePrice": 2000,
  "BasePrice": 1000,
  "SupplierName": "soupper",
  "Name": "San pham Test",
  "Description": "Description testt",
  "Volume": "2x2x2",
  "ManufactureBarcode": null,
  "SellerId": "30788",
  "Quantity": 200,
  "ModelName": "moel",
  "SupplierId": null,
  "ModelId": null,
  "ExternalUrl": null,
  "CategoryId": null,
  "CategoryName": "Dien Thoai",
  "QuantityUnit": 1,
  "SellerSKU": "Biaaaa",
  "Weight": 200,
  "BrandId": "",
  "InventoryId": "36",
  "ProductTags": "tag 1 tag 2",
  "ProductId": "11584"
}
```

### HTTP Request

`POST http://prod.boxme.vn/api/public/bxapi/product-sdk`

### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
ApiKey        | false   | You Key API.
SellerSKU     | false   | Product code (sku) to edit.
InventoryId   | false   | Id inventory (not boxme).
Name          | false   | Product Name.
CategoryName  | false   | Product CategoryName .
SupplierName  | false   | Product SupplierName  .
BrandName     | false   | Product Brand.
Description   | false   | Product Description.
ProductTags   | false   | Product tag.
Quantity      | false   | Product quantity.
BasePrice     | false   | New product base price.
SalePrice     | false   | New product sale price (Price to create orders) .
Weight        | false   | New product weight (weight to create orders).
BarcodeManufacturer     | false   | Barcode (if exist).
ProductImages | false   | Images (if exist) .

<aside class="notice">
You must with your personal API key.
</aside>


## Get Lists Product

> To authorize, use this code:

```ruby
require 'uri'
require 'net/http'

url = URI("http://prod.boxme.vn/api/public/bxapi/list_product-sdk")

http = Net::HTTP.new(url.host, url.port)

request = Net::HTTP::Get.new(url)
request["content-type"] = 'application/json'
request.body = "{}"

response = http.request(request)
puts response.read_body
```

```python
import http.client

conn = http.client.HTTPConnection("prod.boxme.vn")

payload = "{}"

headers = { 'content-type': "application/json" }

conn.request("GET", "/api/public/bxapi/list_product-sdk", payload, headers)

res = conn.getresponse()
```

```shell
# With shell, you can just pass the correct header with each request
curl --request GET \
  --url http://prod.boxme.vn/api/public/bxapi/list_product-sdk \
  --header 'content-type: application/json' \
  --data '{}'
```

```javascript
var data = JSON.stringify({});

var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function () {
  if (this.readyState === this.DONE) {
    console.log(this.responseText);
  }
});

xhr.open("GET", "http://prod.boxme.vn/api/public/bxapi/list_product-sdk");
xhr.setRequestHeader("content-type", "application/json");

xhr.send(data);
```
> The above command returns JSON structured like this:

```json
{
  "total_items": 26,
  "data": {
    "product": [
      {
        "Name": "iphone 9 ",
        "InventoryName": "Giau test",
        "SalePrice": 200000,
        "BasePrice": 10000,
        "Description": "Iphone 7s black new 100%",
        "SellerSKU": "A-377AAEE",
        "Weight": 1223,
        "ProductImages": [],
        "InventoryId": 137123,
        "Quantity": 1,
        "ProductId": "89034",
        "ToBoxme": false
      }
    ]
  }
}
```

### HTTP Request

`GET http://prod.boxme.vn/api/public/bxapi/list_product-sdk`

### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
ApiKey        | false   | You Key API.

<aside class="notice">
You must with your personal API key.
</aside>

## Get List Product To Create Order

> To authorize, use this code:

```ruby
require 'uri'
require 'net/http'

url = URI("http://prod.boxme.vn/api/public/bxapi/get_list_products_create_order")

http = Net::HTTP.new(url.host, url.port)

request = Net::HTTP::Get.new(url)
request["content-type"] = 'application/json'
request.body = "{}"

response = http.request(request)
puts response.read_body
```

```python
import http.client

conn = http.client.HTTPConnection("prod.boxme.vn")

payload = "{}"

headers = { 'content-type': "application/json" }

conn.request("GET", "/api/public/bxapi/get_list_products_create_order", payload, headers)

res = conn.getresponse()
data = res.read()

print(data.decode("utf-8"))
```

```shell
# With shell, you can just pass the correct header with each request
curl --request GET \
  --url http://prod.boxme.vn/api/public/bxapi/get_list_products_create_order \
  --header 'content-type: application/json' \
  --data '{}'
```

```javascript
var data = JSON.stringify({});

var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function () {
  if (this.readyState === this.DONE) {
    console.log(this.responseText);
  }
});

xhr.open("GET", "http://prod.boxme.vn/api/public/bxapi/get_list_products_create_order");
xhr.setRequestHeader("content-type", "application/json");

xhr.send(data);
```
> The above command returns JSON structured like this:

```json
{
  "total_items": 1,
  "data": {
    "product": [
      {
        "InventoryName": "Vĩnh Lộc - HCM",
        "Waiting": 0,
        "InventoryLimit": 0,
        "SalePrice": "150000",
        "SellerSKU": "PO-150M4NU",
        "Weight": 200,
        "InventoryId": 133902,
        "Name": "Đồng hồ CẶP thời trang Pollock + 2 Pin dự phòng",
        "Quantity": 24,
        "ProductId": "88954"
      }
    ]
  }
}
```

### HTTP Request

`GET http://prod.boxme.vn/api/public/bxapi/get_list_products_create_order`

### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
ApiKey        | false   | You Key API.
inventory_id  | false   | ID inventory(boxme) (non-mandatory).
sellerSKU     | false   | SKU (non-mandatory).

<aside class="notice">
You must with your personal API key.
</aside>