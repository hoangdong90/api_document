# Shipment
## Create Shipment
> To authorize, use this code:

```ruby
require 'uri'
require 'net/http'

url = URI("http://prod.boxme.vn/api/public/bxapi/shipment-sdk")

http = Net::HTTP.new(url.host, url.port)

request = Net::HTTP::Post.new(url)
request["content-type"] = 'application/json'
request.body = "{\"ApiKey\":\"7e4e80c6bdb03c5a2d639c4828bcf156\",\"ShipToAddress\":{\"InventoryId\":133455},\"ShipFromAddress\":{\"InventoryId\":105341},\"TypeTransport\":1,\"ShipmentStatus\":\"ReadyToShip\",\"Volumes\":\"10x10x10\",\"Weight\":500,\"ShippingMethod\":\"MYSELF\",\"ShipmentItems\":[{\"SKU\":\"14398602\",\"QuantityShipped\":100}]}"

response = http.request(request)
puts response.read_body
```

```python
import http.client

conn = http.client.HTTPConnection("prod.boxme.vn")

payload = "{\"ApiKey\":\"7e4e80c6bdb03c5a2d639c4828bcf156\",\"ShipToAddress\":{\"InventoryId\":133455},\"ShipFromAddress\":{\"InventoryId\":105341},\"TypeTransport\":1,\"ShipmentStatus\":\"ReadyToShip\",\"Volumes\":\"10x10x10\",\"Weight\":500,\"ShippingMethod\":\"MYSELF\",\"ShipmentItems\":[{\"SKU\":\"14398602\",\"QuantityShipped\":100}]}"

headers = { 'content-type': "application/json" }

conn.request("POST", "/api/public/bxapi/shipment-sdk", payload, headers)

res = conn.getresponse()
data = res.read()

print(data.decode("utf-8"))
```

```shell
# With shell, you can just pass the correct header with each request
curl --request POST \
  --url http://prod.boxme.vn/api/public/bxapi/shipment-sdk \
  --header 'content-type: application/json' \
  --data '{"ApiKey":"7e4e80c6bdb03c5a2d639c4828bcf156","ShipToAddress":{"InventoryId":133455},"ShipFromAddress":{"InventoryId":105341},"TypeTransport":1,"ShipmentStatus":"ReadyToShip","Volumes":"10x10x10","Weight":500,"ShippingMethod":"MYSELF","ShipmentItems":[{"SKU":"14398602","QuantityShipped":100}]}'
```

```javascript
var data = JSON.stringify({
  "ApiKey": "7e4e80c6bdb03c5a2d639c4828bcf156",
  "ShipToAddress": {
    "InventoryId": 133455
  },
  "ShipFromAddress": {
    "InventoryId": 105341
  },
  "TypeTransport": 1,
  "ShipmentStatus": "ReadyToShip",
  "Volumes": "10x10x10",
  "Weight": 500,
  "ShippingMethod": "MYSELF",
  "ShipmentItems": [
    {
      "SKU": "14398602",
      "QuantityShipped": 100
    }
  ]
});

var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function () {
  if (this.readyState === this.DONE) {
    console.log(this.responseText);
  }
});

xhr.open("POST", "http://prod.boxme.vn/api/public/bxapi/shipment-sdk");
xhr.setRequestHeader("content-type", "application/json");

xhr.send(data);
```
> The above command returns JSON structured like this:

```json
{
  "inventory": [
    {
      "City": "TP. Hồ Chí Minh",
      "Code": null,
      "Name": "Kho - Huyện Nhà Bè - TP.Hồ Chí Minh",
      "District": "Huyện Nhà Bè",
      "Phone": "0938131593",
      "InventoryId": "92932",
      "Type": 0,
      "AddressLine": "aaa"
    }
  ]
}
```

### HTTP Request

`POST http://prod.boxme.vn/api/public/bxapi/shipment-sdk`

### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
ApiKey   | false   | Your api key of seller.

<aside class="notice">
You must with your personal API key.
</aside>