# Orders
## Calculate Shipping fee
> To authorize, use this code:

```ruby
require 'uri'
require 'net/http'

url = URI("http://prod.boxme.vn/api/public/api/rest/courier/calculate")

http = Net::HTTP.new(url.host, url.port)

request = Net::HTTP::Post.new(url)
request["content-type"] = 'application/json'
request.body = "{\"From\":{\"City\":18,\"Province\":167},\"To\":{\"City\":54,\"Province\":587},\"Order\":{\"Amount\":503400,\"Weight\":2000},\"Config\":{\"Service\":2,\"CoD\":1,\"Protected\":1,\"Checking\":1,\"Payment\":2,\"Fragile\":2},\"Domain\":\"haravan.com\",\"MerchantKey\":\"e80d1bb5cde172364fdd6c338b8966ac\"}"

response = http.request(request)
puts response.read_body
```

```python
import http.client

conn = http.client.HTTPConnection("prod.boxme.vn")

payload = "{\"From\":{\"City\":18,\"Province\":167},\"To\":{\"City\":54,\"Province\":587},\"Order\":{\"Amount\":503400,\"Weight\":2000},\"Config\":{\"Service\":2,\"CoD\":1,\"Protected\":1,\"Checking\":1,\"Payment\":2,\"Fragile\":2},\"Domain\":\"haravan.com\",\"MerchantKey\":\"e80d1bb5cde172364fdd6c338b8966ac\"}"

headers = { 'content-type': "application/json" }

conn.request("POST", "/api/public/api/rest/courier/calculate", payload, headers)

res = conn.getresponse()
data = res.read()

print(data.decode("utf-8"))
```

```shell
# With shell, you can just pass the correct header with each request
curl --request POST \
  --url http://prod.boxme.vn/api/public/api/rest/courier/calculate \
  --header 'content-type: application/json' \
  --data '{"From":{"City":18,"Province":167},"To":{"City":54,"Province":587},"Order":{"Amount":503400,"Weight":2000},"Config":{"Service":2,"CoD":1,"Protected":1,"Checking":1,"Payment":2,"Fragile":2},"Domain":"haravan.com","MerchantKey":"e80d1bb5cde172364fdd6c338b8966ac"}'
```

```javascript
var data = JSON.stringify({
  "From": {
    "City": 18,
    "Province": 167
  },
  "To": {
    "City": 54,
    "Province": 587
  },
  "Order": {
    "Amount": 503400,
    "Weight": 2000
  },
  "Config": {
    "Service": 2,
    "CoD": 1,
    "Protected": 1,
    "Checking": 1,
    "Payment": 2,
    "Fragile": 2
  },
  "Domain": "haravan.com",
  "MerchantKey": "e80d1bb5cde172364fdd6c338b8966ac"
});

var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function () {
  if (this.readyState === this.DONE) {
    console.log(this.responseText);
  }
});

xhr.open("POST", "http://prod.boxme.vn/api/public/api/rest/courier/calculate");
xhr.setRequestHeader("content-type", "application/json");

xhr.send(data);
```
> The above command returns JSON structured like this:

```json
{
  "error": "success",
  "error_message": "Tính phí thành công",
  "data": {
    "fee": {
      "total_fee": 0,
      "vas": {
        "protected": 0
      },
      "collect": 250000,
      "base_collect": 250000,
      "total_vas": 0,
      "seller": {
        "pvc": 0,
        "pcod": 0
      },
      "discount": {
        "seller": 0
      }
    },
    "inventory": 24531,
    "courier": {
      "system": {
        "courier_id": 1,
        "courier_name": "Viettel Post",
        "money_pickup": 10000,
        "money_delivery": 0,
        "leatime_pickup": 54809,
        "optional": 0,
        "leatime_delivery": 36,
        "leatime_ward": 0,
        "leatime_total": 54845
      }
    }
  }
}
```

### HTTP Request

`POST http://prod.boxme.vn/api/public/api/rest/courier/calculate`

### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
ApiKey (MerchantKey)    | false   | Your api key of seller.
From.City       | false   | From city send order.
From.Province   | false   | From province send order.
From.Province   | false   | From province send order.
To.City         | false   | To city receive order.
To.Province     | false   | To province receive order.
Order.Amount    | false   | Order amount.
Order.Weight    | false   | Order weight.
Config.Service  | false   | Shipping method:1. Economy delivery service,2. Express delivery service.
Config.CoD      | false   | Cash On Delivery (COD) 1: Use; 2: Don’t use.
Config.Protected| false   | Insurance.
Config.Checking | false   | Accept receiver view item in order before confirm received or not.
Config.Payment  | false   | Payment.
Config.Fragile  | false   | Fragile.

<aside class="notice">
You must with your personal API key.
</aside>

## Create Order Fulfillment (Boxme)
> To authorize, use this code:

```ruby
require 'uri'
require 'net/http'

url = URI("http://prod.boxme.vn/api/public/api/rest/courier/create")

http = Net::HTTP.new(url.host, url.port)

request = Net::HTTP::Post.new(url)
request["content-type"] = 'application/json'
request.body = "{\"Domain\":\"boxme.vn\",\"MerchantKey\":\"e80d1bb5cde172364fdd6c338b89661ac\",\"From\":{\"City\":52,\"Province\":551,\"Stock\":104047,\"Ward\":4394,\"Address\":\"89A Thăng Long, Phường 4, Quận Tân bình, TP Hồ Chí Minh\",\"Phone\":\"1668669897\",\"Name\":\"Nguyễn Anh Bảo Quốc\"},\"Courier\":1,\"To\":{\"City\":46,\"Province\":496,\"Address\":\"TRƯỜNG MẪU GIÁO TÂN THƯỢNG - Thôn 2, Xã Tân Thượng, Huyện Di Linh Xã Tân Thượng, Huyện Di Linh, Lâm Đồng\",\"Country\":237,\"Ward\":\"5243\",\"Phone\":\"0914392331\",\"PhoneCode\":\"84\",\"Name\":\"Nguyễn Thị Sen\"},\"Items\":[{\"Name\":\"Cám Gạo Tinh Nghệ Mật Ong Nguyên Chất\",\"Price\":700000,\"Quantity\":22,\"Weight\":600,\"BSIN\":\"CGT-12\"}],\"Order\":{\"Weight\":600,\"Amount\":700000,\"Quantity\":2,\"Collect\":\"400000\",\"ProductName\":\"Cám Gạo Tinh Nghệ Mật Ong Nguyên Chất\"},\"Config\":{\"Service\":1,\"Protected\":2,\"Checking\":1,\"Fragile\":2,\"CoD\":1,\"Payment\":1,\"AutoAccept\":1}}"

response = http.request(request)
puts response.read_body
```

```python
import http.client

conn = http.client.HTTPConnection("prod.boxme.vn")

payload = "{\"Domain\":\"boxme.vn\",\"MerchantKey\":\"e80d1bb5cde172364fdd6c338b89661ac\",\"From\":{\"City\":52,\"Province\":551,\"Stock\":104047,\"Ward\":4394,\"Address\":\"89A Thăng Long, Phường 4, Quận Tân bình, TP Hồ Chí Minh\",\"Phone\":\"1668669897\",\"Name\":\"Nguyễn Anh Bảo Quốc\"},\"Courier\":1,\"To\":{\"City\":46,\"Province\":496,\"Address\":\"TRƯỜNG MẪU GIÁO TÂN THƯỢNG - Thôn 2, Xã Tân Thượng, Huyện Di Linh Xã Tân Thượng, Huyện Di Linh, Lâm Đồng\",\"Country\":237,\"Ward\":\"5243\",\"Phone\":\"0914392331\",\"PhoneCode\":\"84\",\"Name\":\"Nguyễn Thị Sen\"},\"Items\":[{\"Name\":\"Cám Gạo Tinh Nghệ Mật Ong Nguyên Chất\",\"Price\":700000,\"Quantity\":22,\"Weight\":600,\"BSIN\":\"CGT-12\"}],\"Order\":{\"Weight\":600,\"Amount\":700000,\"Quantity\":2,\"Collect\":\"400000\",\"ProductName\":\"Cám Gạo Tinh Nghệ Mật Ong Nguyên Chất\"},\"Config\":{\"Service\":1,\"Protected\":2,\"Checking\":1,\"Fragile\":2,\"CoD\":1,\"Payment\":1,\"AutoAccept\":1}}"

headers = { 'content-type': "application/json" }

conn.request("POST", "/api/public/api/rest/courier/create", payload, headers)

res = conn.getresponse()
data = res.read()

print(data.decode("utf-8"))
```

```shell
# With shell, you can just pass the correct header with each request
curl --request POST \
  --url http://prod.boxme.vn/api/public/api/rest/courier/create \
  --header 'content-type: application/json' \
  --data '{"Domain":"boxme.vn","MerchantKey":"e80d1bb5cde172364fdd6c338b89661ac","From":{"City":52,"Province":551,"Stock":104047,"Ward":4394,"Address":"89A Thăng Long, Phường 4, Quận Tân bình, TP Hồ Chí Minh","Phone":"1668669897","Name":"Nguyễn Anh Bảo Quốc"},"Courier":1,"To":{"City":46,"Province":496,"Address":"TRƯỜNG MẪU GIÁO TÂN THƯỢNG - Thôn 2, Xã Tân Thượng, Huyện Di Linh Xã Tân Thượng, Huyện Di Linh, Lâm Đồng","Country":237,"Ward":"5243","Phone":"0914392331","PhoneCode":"84","Name":"Nguyễn Thị Sen"},"Items":[{"Name":"Cám Gạo Tinh Nghệ Mật Ong Nguyên Chất","Price":700000,"Quantity":22,"Weight":600,"BSIN":"CGT-12"}],"Order":{"Weight":600,"Amount":700000,"Quantity":2,"Collect":"400000","ProductName":"Cám Gạo Tinh Nghệ Mật Ong Nguyên Chất"},"Config":{"Service":1,"Protected":2,"Checking":1,"Fragile":2,"CoD":1,"Payment":1,"AutoAccept":1}}'
```

```javascript
var data = JSON.stringify({
  "Domain": "boxme.vn",
  "MerchantKey": "e80d1bb5cde172364fdd6c338b89661ac",
  "From": {
    "City": 52,
    "Province": 551,
    "Stock": 104047,
    "Ward": 4394,
    "Address": "89A Thăng Long, Phường 4, Quận Tân bình, TP Hồ Chí Minh",
    "Phone": "1668669897",
    "Name": "Nguyễn Anh Bảo Quốc"
  },
  "Courier": 1,
  "To": {
    "City": 46,
    "Province": 496,
    "Address": "TRƯỜNG MẪU GIÁO TÂN THƯỢNG - Thôn 2, Xã Tân Thượng, Huyện Di Linh Xã Tân Thượng, Huyện Di Linh, Lâm Đồng",
    "Country": 237,
    "Ward": "5243",
    "Phone": "0914392331",
    "PhoneCode": "84",
    "Name": "Nguyễn Thị Sen"
  },
  "Items": [
    {
      "Name": "Cám Gạo Tinh Nghệ Mật Ong Nguyên Chất",
      "Price": 700000,
      "Quantity": 22,
      "Weight": 600,
      "BSIN": "CGT-12"
    }
  ],
  "Order": {
    "Weight": 600,
    "Amount": 700000,
    "Quantity": 2,
    "Collect": "400000",
    "ProductName": "Cám Gạo Tinh Nghệ Mật Ong Nguyên Chất"
  },
  "Config": {
    "Service": 1,
    "Protected": 2,
    "Checking": 1,
    "Fragile": 2,
    "CoD": 1,
    "Payment": 1,
    "AutoAccept": 1
  }
});

var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function () {
  if (this.readyState === this.DONE) {
    console.log(this.responseText);
  }
});

xhr.open("POST", "http://prod.boxme.vn/api/public/api/rest/courier/create");
xhr.setRequestHeader("content-type", "application/json");

xhr.send(data);
```
> The above command returns JSON structured like this:

```json
{
  "error": "success",
  "error_message": "Tạo vận đơn thành công",
  "data": {
    "TrackingCode": "SC5732160351",
    "CourierId": 1,
    "MoneyCollect": 250000,
    "ShowFee": {
      "pvc": 0,
      "cod": 0,
      "pbh": 0
    },
    "Discount": {
      "seller": 0
    }
  }
}
```

### HTTP Request

`POST http://prod.boxme.vn/api/public/api/rest/courier/create`

### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
ApiKey (MerchantKey)    | false   | Your api key of seller.
Courier         | false   | Courier.
From.City       | false   | City inventory.
From.Province   | false   | Province inventoryr.
From.Stock      | false   | Id inventory.
From.Ward       | false   | Ward inventory.
From.Address    | false   | Address inventory.
From.Phone      | false   | Phone seller.
From.Name       | false   | Name seller.
To.City         | false   | City receive order.
To.Province     | false   | Province receive order.
To.Country      | false   | Country receive order.
To.Ward         | false   | Ward receive order.
To.Address      | false   | Address receive order.
To.Phone        | false   | Phone buyer.
To.Name         | false   | Name buyer.
To.PhoneCode    | false   | Name seller.
Items.Name      | false   | Name product.
Items.Price     | false   | Price product.
Items.Quantity    | false   | Quantity product.
Items.Weight    | false   | Weight product.
Items.BSIN      | false   | BSIN product.
Order.Weight      | false   | Total weight of item in order.
Order.Amount      | false   | Total product value.
Order.Quantity      | false   | Total quantity of item in order.
Order.Collect      | false   | Total money collect from buyer( when you use CoD). Currency: VND.
Order.ProductName      | false   | Info orders.
Config.Service  | false   | Shipping method:1. Economy delivery service,2. Express delivery service.
Config.CoD      | false   | Cash On Delivery (COD) 1: Use; 2: Don’t use.
Config.Protected| false   | Insurance.
Config.Checking | false   | Accept receiver view item in order before confirm received or not.
Config.Payment  | false   | Payment.
Config.Fragile  | false   | Fragile.
Config.AutoAccept  | false   | 1.Auto accept 0. No accept (default).

<aside class="notice">
You must with your personal API key.
</aside>

## Create Shipping Order (Only Ship)
> To authorize, use this code:

```ruby
require 'uri'
require 'net/http'

url = URI("http://prod.boxme.vn/api/public/api/rest/courier/create")

http = Net::HTTP.new(url.host, url.port)

request = Net::HTTP::Post.new(url)
request["content-type"] = 'application/json'
request.body = "{\"From\":{\"City\":18,\"Province\":167,\"Ward\":479,\"Name\":\"Fashion Collections\",\"Phone\":\"123123123\",\"Address\":\"123/67 PVH\"},\"To\":{\"City\":54,\"Province\":587,\"Ward\":11250,\"Name\":\"HCK\",\"Phone\":\"123123123123\",\"Address\":\"123/67 Phan Van Hon\"},\"Order\":{\"Amount\":503400,\"Weight\":2000,\"Code\":4,\"Quantity\":1,\"ProductName\":\"Leather shoes (M)\"},\"Type\":\"excel\",\"Config\":{\"Service\":2,\"CoD\":1,\"Protected\":1,\"Checking\":1,\"Payment\":2,\"Fragile\":2,\"AutoAccept\":0},\"Domain\":\"haravan.com\",\"MerchantKey\":\"e80d1bb5cde172364fdd6c338b8966ac\"}"

response = http.request(request)
```

```python
import http.client

conn = http.client.HTTPConnection("prod.boxme.vn")

payload = "{\"From\":{\"City\":18,\"Province\":167,\"Ward\":479,\"Name\":\"Fashion Collections\",\"Phone\":\"123123123\",\"Address\":\"123/67 PVH\"},\"To\":{\"City\":54,\"Province\":587,\"Ward\":11250,\"Name\":\"HCK\",\"Phone\":\"123123123123\",\"Address\":\"123/67 Phan Van Hon\"},\"Order\":{\"Amount\":503400,\"Weight\":2000,\"Code\":4,\"Quantity\":1,\"ProductName\":\"Leather shoes (M)\"},\"Type\":\"excel\",\"Config\":{\"Service\":2,\"CoD\":1,\"Protected\":1,\"Checking\":1,\"Payment\":2,\"Fragile\":2,\"AutoAccept\":0},\"Domain\":\"haravan.com\",\"MerchantKey\":\"e80d1bb5cde172364fdd6c338b8966ac\"}"

headers = { 'content-type': "application/json" }

conn.request("POST", "/api/public/api/rest/courier/create", payload, headers)

res = conn.getresponse()
data = res.read()

print(data.decode("utf-8"))
```

```shell
# With shell, you can just pass the correct header with each request
curl --request POST \
  --url http://prod.boxme.vn/api/public/api/rest/courier/create \
  --header 'content-type: application/json' \
  --data '{"From":{"City":18,"Province":167,"Ward":479,"Name":"Fashion Collections","Phone":"123123123","Address":"123/67 PVH"},"To":{"City":54,"Province":587,"Ward":11250,"Name":"HCK","Phone":"123123123123","Address":"123/67 Phan Van Hon"},"Order":{"Amount":503400,"Weight":2000,"Code":4,"Quantity":1,"ProductName":"Leather shoes (M)"},"Type":"excel","Config":{"Service":2,"CoD":1,"Protected":1,"Checking":1,"Payment":2,"Fragile":2,"AutoAccept":0},"Domain":"haravan.com","MerchantKey":"e80d1bb5cde172364fdd6c338b8966ac"}'
```

```javascript
var data = JSON.stringify({
  "From": {
    "City": 18,
    "Province": 167,
    "Ward": 479,
    "Name": "Fashion Collections",
    "Phone": "123123123",
    "Address": "123/67 PVH"
  },
  "To": {
    "City": 54,
    "Province": 587,
    "Ward": 11250,
    "Name": "HCK",
    "Phone": "123123123123",
    "Address": "123/67 Phan Van Hon"
  },
  "Order": {
    "Amount": 503400,
    "Weight": 2000,
    "Code": 4,
    "Quantity": 1,
    "ProductName": "Leather shoes (M)"
  },
  "Type": "excel",
  "Config": {
    "Service": 2,
    "CoD": 1,
    "Protected": 1,
    "Checking": 1,
    "Payment": 2,
    "Fragile": 2,
    "AutoAccept": 0
  },
  "Domain": "haravan.com",
  "MerchantKey": "e80d1bb5cde172364fdd6c338b8966ac"
});

var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function () {
  if (this.readyState === this.DONE) {
    console.log(this.responseText);
  }
});

xhr.open("POST", "http://prod.boxme.vn/api/public/api/rest/courier/create");
xhr.setRequestHeader("content-type", "application/json");

xhr.send(data);

```
> The above command returns JSON structured like this:

```json
{
  "error": "success",
  "error_message": "Tạo vận đơn thành công",
  "data": {
    "TrackingCode": "SC5732160351",
    "CourierId": 1,
    "MoneyCollect": 250000,
    "ShowFee": {
      "pvc": 0,
      "cod": 0,
      "pbh": 0
    },
    "Discount": {
      "seller": 0
    }
  }
}
```

### HTTP Request

`POST http://prod.boxme.vn/api/public/api/rest/courier/create`

### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
ApiKey (MerchantKey)    | false   | Your api key of seller.
Courier         | false   | Courier.
From.City       | false   | City inventory.
From.Province   | false   | Province inventoryr.
From.Stock      | false   | Id inventory.
From.Ward       | false   | Ward inventory.
From.Address    | false   | Address inventory.
From.Phone      | false   | Phone seller.
From.Name       | false   | Name seller.
To.City         | false   | City receive order.
To.Province     | false   | Province receive order.
To.Country      | false   | Country receive order.
To.Ward         | false   | Ward receive order.
To.Address      | false   | Address receive order.
To.Phone        | false   | Phone buyer.
To.Name         | false   | Name buyer.
To.PhoneCode    | false   | Name seller.
Order.Weight      | false   | Total weight of item in order.
Order.Amount      | false   | Total product value.
Order.Quantity      | false   | Total quantity of item in order.
Order.Collect      | false   | Total money collect from buyer( when you use CoD). Currency: VND.
Order.ProductName      | false   | Info orders.
Config.Service  | false   | Shipping method:1. Economy delivery service,2. Express delivery service.
Config.CoD      | false   | Cash On Delivery (COD) 1: Use; 2: Don’t use.
Config.Protected| false   | Insurance.
Config.Checking | false   | Accept receiver view item in order before confirm received or not.
Config.Payment  | false   | Payment.
Config.Fragile  | false   | Fragile.
Config.AutoAccept  | false   | 1.Auto accept 0. No accept (default).
Type  | false   | "Type": "excel" : We auto select best courier. You don't define params Courier.

<aside class="notice">
You must with your personal API key.
</aside>

## Cancel Order
> To authorize, use this code:

```ruby
require 'uri'
require 'net/http'

url = URI("http://prod.boxme.vn/api/public/api/rest/lading/cancel")

http = Net::HTTP.new(url.host, url.port)

request = Net::HTTP::Post.new(url)
request["content-type"] = 'application/json'
request.body = "{\"TrackingCode\":\"SC52078917691\",\"ApiKey\":\"string (required)\"}"

response = http.request(request)
puts response.read_body
```

```python
import requests

url = "http://prod.boxme.vn/api/public/api/rest/lading/cancel"

payload = "{\"TrackingCode\":\"SC52078917691\",\"ApiKey\":\"string (required)\"}"
headers = {'content-type': 'application/json'}

response = requests.request("POST", url, data=payload, headers=headers)

print(response.text)
```

```shell
# With shell, you can just pass the correct header with each request
curl --request POST \
  --url http://prod.boxme.vn/api/public/api/rest/lading/cancel \
  --header 'content-type: application/json' \
  --data '{"TrackingCode":"SC52078917691","ApiKey":"string (required)"}'
```

```javascript
var data = JSON.stringify({
  "TrackingCode": "SC52078917691",
  "ApiKey": "string (required)"
});

var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function () {
  if (this.readyState === this.DONE) {
    console.log(this.responseText);
  }
});

xhr.open("POST", "http://prod.boxme.vn/api/public/api/rest/lading/cancel");
xhr.setRequestHeader("content-type", "application/json");

xhr.send(data);

```
> The above command returns JSON structured like this:


### HTTP Request

`POST http://prod.boxme.vn/api/public/api/rest/lading/cancel`

### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
ApiKey (MerchantKey)    | false   | Your api key of seller.
TrackingCode         | false   | Shipchung Tracking Code.

<aside class="notice">
You must with your personal API key.
</aside>

## Accept Order
> To authorize, use this code:

```ruby
require 'uri'
require 'net/http'

url = URI("http://prod.boxme.vn/api/public/api/merchant/rest/lading/accept")

http = Net::HTTP.new(url.host, url.port)

request = Net::HTTP::Post.new(url)
request["content-type"] = 'application/json'
request.body = "{\"TrackingCode\":\"string (optional)\",\"ApiKey\":\"string (optional)\"}"

response = http.request(request)
puts response.read_body
```

```python
import requests

url = "http://prod.boxme.vn/api/public/api/merchant/rest/lading/accept"

payload = "{\"TrackingCode\":\"string (optional)\",\"ApiKey\":\"string (optional)\"}"
headers = {'content-type': 'application/json'}

response = requests.request("POST", url, data=payload, headers=headers)

print(response.text)
```

```shell
# With shell, you can just pass the correct header with each request
curl --request POST \
  --url http://prod.boxme.vn/api/public/api/merchant/rest/lading/accept \
  --header 'content-type: application/json' \
  --data '{"TrackingCode":"string (optional)","ApiKey":"string (optional)"}'
```

```javascript
var data = JSON.stringify({
  "TrackingCode": "string (optional)",
  "ApiKey": "string (optional)"
});

var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function () {
  if (this.readyState === this.DONE) {
    console.log(this.responseText);
  }
});

xhr.open("POST", "http://prod.boxme.vn/api/public/api/merchant/rest/lading/accept");
xhr.setRequestHeader("content-type", "application/json");

xhr.send(data);

```
> The above command returns JSON structured like this:

```json
{
  "error": true,
  "error_message": {
    "TrackingCode": [
      "The tracking code format is invalid."
    ]
  }
}
```
### HTTP Request

`POST http://prod.boxme.vn/api/public/api/merchant/rest/lading/accept`

### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
ApiKey (MerchantKey)    | false   | Your api key of seller.
TrackingCode         | false   | Shipchung Tracking Code.

<aside class="notice">
You must with your personal API key.
</aside>


## Merchant receive order status
This is webhook api. 
We will push order status and journey of order to your system via RestApi. 
Config at:

https://seller.boxme.vn/#/app/config/key

https://seller.shipchung.vn/#/app/config/key
> To authorize, use this code:

```ruby
require 'uri'
require 'net/http'

url = URI("http://prod.boxme.vn/api/public/your-webhook-api")

http = Net::HTTP.new(url.host, url.port)

request = Net::HTTP::Post.new(url)
request["http_x_shipchung_refesh_token"] = 'your key'
request["content-type"] = 'application/json'
request.body = "{\"TrackingCode\":\"SC5238848343\",\"StatusId\":\"string (required)\",\"StatusName\":\"string (required)\",\"TimeStamp\":\"integer (required)\"}"
```

```python
import requests

url = "http://prod.boxme.vn/api/public/your-webhook-api"

payload = "{\"TrackingCode\":\"SC5238848343\",\"StatusId\":\"string (required)\",\"StatusName\":\"string (required)\",\"TimeStamp\":\"integer (required)\"}"
headers = {
    'http_x_shipchung_refesh_token': "your key",
    'content-type': "application/json"
    }

response = requests.request("POST", url, data=payload, headers=headers)
```

```shell
# With shell, you can just pass the correct header with each request
curl --request POST \
  --url http://prod.boxme.vn/api/public/your-webhook-api \
  --header 'content-type: application/json' \
  --header 'http_x_shipchung_refesh_token: your key' \
  --data '{"TrackingCode":"SC5238848343","StatusId":"string (required)","StatusName":"string (required)","TimeStamp":"integer (required)"}'
```

```javascript
var data = JSON.stringify({
  "TrackingCode": "SC5238848343",
  "StatusId": "string (required)",
  "StatusName": "string (required)",
  "TimeStamp": "integer (required)"
});

var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function () {
  if (this.readyState === this.DONE) {
    console.log(this.responseText);
  }
});

xhr.open("POST", "http://prod.boxme.vn/api/public/your-webhook-api");
xhr.setRequestHeader("http_x_shipchung_refesh_token", "your key");
xhr.setRequestHeader("content-type", "application/json");

xhr.send(data);

```
> The above command returns JSON structured like this:

```json
{
  "error": true,
  "error_message": {
    "TrackingCode": [
      "The tracking code format is invalid."
    ]
  }
}
```
### HTTP Request

`POST http://prod.boxme.vn/api/public/your-webhook-api`

### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
ApiKey (MerchantKey)    | false   | Your api key of seller.
TrackingCode            | false   | Shipchung Tracking Code.
StatusId                | false   | Status Code defined status api.
StatusName              | false   | Status name.
TimeStamp               | false   | Time update this status.

<aside class="notice">
You must with your personal API key.
</aside>

## List Status Order
This is webhook api. 
We will push order status and journey of order to your system via RestApi. 
Config at:

https://seller.boxme.vn/#/app/config/key

https://seller.shipchung.vn/#/app/config/key
> To authorize, use this code:

```ruby
require 'uri'
require 'net/http'

url = URI("http://prod.boxme.vn/api/public/api/merchant/rest/lading/status")

http = Net::HTTP.new(url.host, url.port)

request = Net::HTTP::Get.new(url)
request.body = "{}"

response = http.request(request)
puts response.read_body
```

```python
import requests

url = "http://prod.boxme.vn/api/public/api/merchant/rest/lading/status"

payload = "{}"
response = requests.request("GET", url, data=payload)

print(response.text)
```

```shell
# With shell, you can just pass the correct header with each request
curl --request GET \
  --url http://prod.boxme.vn/api/public/api/merchant/rest/lading/status \
  --data '{}'
```

```javascript
var data = "{}";

var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function () {
  if (this.readyState === this.DONE) {
    console.log(this.responseText);
  }
});

xhr.open("GET", "http://prod.boxme.vn/api/public/api/merchant/rest/lading/status");

xhr.send(data);

```
> The above command returns JSON structured like this:

```json
{
  "error": false,
  "message": "Thành công",
  "data": [
    {
      "StatusId": 12,
      "StatusName": "Chờ duyệt",
      "StatusName_EN": "Waiting approve"
    },
    {
      "StatusId": 13,
      "StatusName": "Đã duyệt",
      "StatusName_EN": "Approved"
    },
    {
      "StatusId": 14,
      "StatusName": "Đang lấy hàng",
      "StatusName_EN": "Picking"
    },
    {
      "StatusId": 15,
      "StatusName": "Lấy không thành công",
      "StatusName_EN": "Pickup failed"
    },
    {
      "StatusId": 16,
      "StatusName": "Đã lấy hàng",
      "StatusName_EN": "Picked"
    },
    {
      "StatusId": 17,
      "StatusName": "Đang phát hàng",
      "StatusName_EN": "Delivering"
    },
    {
      "StatusId": 18,
      "StatusName": "Phát không thành công",
      "StatusName_EN": "Delivery failed"
    },
    {
      "StatusId": 19,
      "StatusName": "Đã phát thành công",
      "StatusName_EN": "Delivered"
    },
    {
      "StatusId": 20,
      "StatusName": "Chờ XN chuyển hoàn",
      "StatusName_EN": "Waiting return"
    },
    {
      "StatusId": 21,
      "StatusName": "Chuyển hoàn",
      "StatusName_EN": "Returning"
    },
    {
      "StatusId": 22,
      "StatusName": "Hủy đơn hàng",
      "StatusName_EN": "Cancelled"
    }
  ]
}
```
### HTTP Request

`GET http://prod.boxme.vn/api/public/api/merchant/rest/lading/status`

<aside class="notice">
You must with your personal API key.
</aside>